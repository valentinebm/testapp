Rails.application.routes.draw do
  devise_for :adeqo_users
  get 'adeqo_users/index'
  get 'adeqo_users/new'
  get 'adeqo_users/create'
 post 'adeqo_users/create'
  get 'adeqo_users/show'
  get 'create/new'
  get 'create/show'
  get 'users/index'
  get 'users/new'
  get 'users/create'
  get 'users/show'
  get 'users/error'
 post 'users/update'
# get 'users/downloadkwdump'
# resources :students
  resources :users
  resources :adeqo_users

  get 'welcome/index'
 #For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
 #root 'application#hello'
 get 'welcome/homepage'

constraints subdomain: "alpha" do
        #get '/'=> redirect { |params| "http://alpha.adeqo.com/users/index" }
        get '/' => 'users#index'
end
  root 'welcome#homepage'

post 'users/downloadkwdump' => "users#downloadkwdump"
post 'users/updatead' => "users#updatead"

 get 'chris/test' => "chris#test"

post 'users/updateaccount' => "users#updateaccount"
post 'users/deleteaccount' => "users#deleteaccount"

post 'adeqo_users/updateuser' => "adeqo_users#updateuser"
post 'adeqo_users/deleteuser' => "adeqo_users#deleteuser"




end
