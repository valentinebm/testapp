require 'test_helper'

class AdeqoUsersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get adeqo_users_index_url
    assert_response :success
  end

  test "should get new" do
    get adeqo_users_new_url
    assert_response :success
  end

  test "should get create" do
    get adeqo_users_create_url
    assert_response :success
  end

  test "should get show" do
    get adeqo_users_show_url
    assert_response :success
  end

end
