#begin API calls

def sogou_login(username, password, token, api_string)
	@sogou_api = Savon.client(
		wsdl: "http://api.agent.sogou.com:80/sem/sms/v1/" + api_string + "?wsdl",
		pretty_print_xml: true,
		log: true,
		env_namespace: :soap,
		namespaces: {"xmlns:common" => "http://api.sogou.com/sem/common/v1"},
		soap_header: {
			"common:AuthHeader" => {
				'common:token' => token,
				'common:username' => username,
				'common:password' => password
			}
		}
        )
end

#sogou api call using Savon

def sogou_api(username, password, token, api_string)
	@sogou_api = Savon.client(
		wsdl: "http://api.agent.sogou.com:80/sem/sms/v1/" + api_string + "?wsdl",
		pretty_print_xml: true,
		log: true,
		env_namespace: :soap,
		namespaces: {"xmlns:common" => "http://api.sogou.com/sem/common/v1"},
		soap_header: {
			"common:AuthHeader" => {
				'common:token' => token,
				'common:username' => username,
				'common:password' => password
			}
		}
	)
end

def sogou_groupid(username, password, apitoken)

        group_id_array = []
        group_id_array_string = ""

        result = sogou_api(username, password, apitoken, "CpcGrpService")
        if result.nil? then return nil end

        update_status = sogou_api.call(:get_all_cpc_grp_id)
        update_status_body = update_status.body.to_hash
        adgroups = update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]

        adgroups.each do |el|
            if el[:cpc_grp_ids].is_a?(Array)
                el[:cpc_grp_ids].each do |id| group_id_array << id end
            else
                group_id_array << el[:cpc_grp_ids]
            end
        end

        if group_id_array.count.to_i > 0
            return group_id_array
        else
            return nil
        end

end
