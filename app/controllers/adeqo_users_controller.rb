class AdeqoUsersController < ApplicationController

def index
	@adeqo_users = AdeqoUser.all
end

def new
	@adeqo_user = AdeqoUser.new
	render :layout => false
end

def create
	@adeqo_user = AdeqoUser.new(params[:adeqo_user].permit!)
	if @adeqo_user.save
		#redirect_to @adeqo_user, alert: "User created successfully."
		flash[:notice] = "You signed up successfully"
		flash[:color]= "valid"
		redirect_to "/adeqo_users"


	else
		#redirect_to adeqo_users_new_path, alert: "Error creating user."
		flash[:notice] = "Form is invalid"
		flash[:color]= "invalid"
		render "new", :layout => false
	end


end

def show
	@adeqo_users = AdeqoUser.find(params[:id])
end


def updateuser
        @adeqo_user =  AdeqoUser.find(params[:adeqo_user][:id])
	if @adeqo_user.update
		redirect_to @adeqo_user, alert: "User updated successfully."
	else
		redirect_to adeqo_users_new_path, alert: "Error updating user."
	end
end


def deleteuser
	@adeqo_user = AdeqoUser.find(params[:id])
	if @adeqo_user.destroy
		redirect_to @adeqo_user, alert: "User deleted successfully."
	else
		redirect_to adeqo_users_new_path, alert: "Error deleting user."
	end
end

end
