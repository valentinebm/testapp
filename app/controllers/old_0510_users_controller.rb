class UsersController < ApplicationController

require "threesixty.rb"
require "sogou.rb"

def index
	@users = User.all
end

def new
	@user = User.new
end

def create
   	@user = User.new(params[:user].permit!)
   	if @user.save
        	redirect_to @user, alert: "User created successfully."
   	else
       		redirect_to new_user_path, alert: "Error creating user."
    	end
end

def show
	@users = User.find(params[:id])
end

def updateaccount
	@user =  User.find(params[:id])
end

def deleteaccount
	@user = User.find(params[:id])
	if @user.destroy
                        redirect_to @user, alert: "User Deleted successfully."
                else
                       redirect_to new_user_path, alert: "Error deleting user."
                end
end

def error
	@error = Error.new(params[:object])
end 

#function takes a USER ID and finds what channel and then calls the corresponding channel ID

def downloadkwdump
                @user = User.find(params[:id])
                if @user.channel == "Sogou"
		  update_sogou(@user.username,@user.password,@user.apitoken)
                        return
                end

                if @user.channel == "360"
                        update_threesixty(@user.username,@user.password,@user.apitoken, @user.apisecret)

                        return
                end

end



#function to take a set of username, password and apitoken and output a list of URLs
#finds all campaigns, then loops within the campaigns to get adgroups, and then loops within the adgroups to get kw
 
def update_sogou(username,password,api_token)

	@username = username
	@password = password
	@api_token = api_token
	@campaign_id = params[:id]


	logger.info "-----AccountService begin-----"

        sogou_api(@username, @password, @api_token, "AccountService")
	sogou_result = @sogou_api.call(:get_account_info)

	@remain_quote = sogou_result.header[:res_header][:rquota].to_i

	@adgroup_id_array = []
	@plan_id_and_plan_name_array = []
	@plan_id_and_group_id_array = []

=begin
#noneed
	logger.info "-----CpcPlanService begin-----"

	sogou_api(@username, @password, @api_token, "CpcPlanService")
	@update_status = @sogou_api.call(:get_all_cpc_plan)
	@header = @update_status.header.to_hash
	#logger.info @msg = @header[:res_header][:desc]
	@remain_quote = @header[:res_header][:rquota]

	@update_status_body = @update_status.body.to_hash
	@result_active_plan = @update_status_body[:get_all_cpc_plan_response][:cpc_plan_types]

	#logger.info "-----plan id and plan name is-----"

	@result_active_plan.each do |el|
	    @plan_id_and_plan_name_array << "Plan ID:" + el[:cpc_plan_id] + ", Plan Name:" + el[:cpc_plan_name]
	end

	logger.info "-----CpcPlanService end-----"
=end

=begin #360 groupids class
#pass a campaign ID, apitoken, access token, return an array of adgroups ID

def threesixty_adgroupid(apitoken, access_token,campaignid)
                                adgroup_id_arr = []
                                adgroup_id_arr_str = ""
                                body = {}
                                body[:campaignId] = campaignid
                                result = threesixty_api( apitoken.to_s, access_token, "group", "getIdListByCampaignId", body)
                                if result.nil? then return nil end
                                adgroups = result["group_getIdListByCampaignId_response"]["groupIdList"]["item"]

                                if adgroups.is_a?(Array)
                                        if adgroups.count.to_i > 0
                                                  adgroups.each do |adgroups_d|
                                                      adgroup_id_arr << adgroups_d.to_i
                                                  end
                                              end
                                          else
                                              adgroup_id_arr << adgroups.to_i
                                          end
                                if adgroup_id_arr.count.to_i > 0
                                        return adgroup_id_arr
                                else
                                        return nil
                                end
end
=end

	logger.info "-----CpcGrpService begin-----"

        sogou_api(@username, @password, @api_token, "CpcGrpService")
        @update_status = @sogou_api.call(:get_all_cpc_grp_id)
	@header = @update_status.header.to_hash
	#logger.info @msg = @header[:res_header][:desc]
	@remain_quote = @header[:res_header][:rquota]

	@update_status_body = @update_status.body.to_hash
	@result_active_grp = @update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]

	#logger.info "-----plan id and group id array is-----"

	@result_active_grp.each do |el|
	    if el[:cpc_grp_ids].is_a?(Array)
	        el[:cpc_grp_ids].each do |id|
	            @adgroup_id_array << id
	        end
	    else
	        @adgroup_id_array << el[:cpc_grp_ids]
	    end
	    #@plan_id_and_group_id_array << "Plan ID:" + el[:cpc_plan_id] + ", Group IDs:" + el[:cpc_grp_ids].to_s
	end

	#logger.info @adgroup_id_array[20106]
	#logger.info @adgroup_id_array.count

	logger.info "-----CpcGrpService end-----"


	logger.info "-----CpcIdeaService begin-----"

	sogou_api(@username, @password, @api_token, "CpcIdeaService")

	@idea_id_and_visit_url_array = []

	#@adgroup_id_array = ["238876896", "238895734", "215628567"] #index 20106, 20107(nil), 20108
	#@adgroup_id_array[20106..-1].each_with_index do |el, index|
	@adgroup_id_array.each_with_index do |el, index|

	    @update_status = @sogou_api.call(:get_cpc_idea_by_cpc_grp_id, message: { cpcGrpIds: el, getTemp: 0 }) 
	    @header = @update_status.header.to_hash
	    #logger.info @msg = @header[:res_header][:desc]
	    @remain_quote = @header[:res_header][:rquota]

	    @update_status_body = @update_status.body.to_hash
	    @result_active_idea = @update_status_body[:get_cpc_idea_by_cpc_grp_id_response][:cpc_grp_ideas]

	    #logger.info "Group ID " + @result_active_idea[:cpc_grp_id]

	    if !@result_active_idea[:cpc_idea_types].nil?
	        @result_active_idea[:cpc_idea_types].each do |idea|
                    @idea_id_and_visit_url_array << "Idea ID:" + idea[:cpc_idea_id] + ", Visit Url:" + idea[:visit_url].to_s
                end
	    else
                @idea_id_and_visit_url_array << "Idea ID: nil" + ", Visit Url: nil"

	    end

	    #@index = index + 20106
	    #logger.info "index is " + @index.to_s
	    logger.info "index is #{index}"

	end


	logger.info "-----CpcIdeaService end-----"

	  @update_status_body = @idea_id_and_visit_url_array
	# @update_status_body = @adgroup_id_array
	# @update_status_body = @result_active_grp
	# @update_status_body = @plan_id_and_plan_name_array
	# @update_status_body = @plan_id_and_group_id_array

	render :json => @update_status_body

	#render plain: "hello"
	#render plain: @adgroup_id_array

	#render :action => "error", :error => @plan_info


	logger.info "-----AccountService end-----"

	rescue
	    logger.info "-----error class-----"
	    logger.info $!.class
	    logger.info $@.class
	    logger.info "-----error message-----"
	    logger.info $!
	    logger.info $@[0]

	    @error = []
	    @error << $i.class << $i << $@.class << $@[0]
	    render plain: @error

end

#Function Takes a User Credential and then calls the ad and kw level. all functions in threesixty.rb Updates the KW URL

def update_threesixty(username,password,apitoken,apisecret)
		@username = username
		@password = password
		@apitoken = apitoken
		@apisecret = apisecret
		login_info = threesixty_login(@username,@password,@apitoken,@apisecret)
		@access_token = login_info["account_clientLogin_response"]["accessToken"]
		campaigncount = 0
		totalchanges = 0
		if !@access_token.nil?
		
			campaigndata = threesixty_campaignids(@apitoken.to_s,@access_token)
			if campaigndata.count.to_i == 0 then return end
			campaigndata[19..-1].each do |campaignid|
			#campaigndata.each do |campaignid|
=begin
				if campaigncount < 10 
					campaigncount = campaigncount +1
					next
					end 
=end
				@adgroup_id_arr = []
				@adgroup_id_arr = threesixty_adgroupid(@apitoken.to_s,@access_token,campaignid)
				if @adgroup_id_arr.nil? then next end 
				@adgroup_id_arr.each do |adgroupid|
						@all_keyword = []
						@all_keyword = threesixty_keywordid(@apitoken.to_s,@access_token,adgroupid)  
					#	logger.info @all_keyword
					 	#logger.info @all_keyword.count
						#break
						delta = 0
						if @all_keyword.nil? then next end  					
								@all_keyword.each do |key|
								if key["destinationUrl"].include? "adeqo"	
									@new_url= URI.unescape(key["destinationUrl"].scan(/=(http[^>]*)/).last.first)
									#logger.info key
									#kw update function
									requesttypearray = []
									request_str = '{"id":'+key["id"].to_s+',"url":"'+@new_url+'"}'
									requesttypearray << request_str
                                                      			kw_update_request = '['+requesttypearray.join(",")+']'
										bodyn = {}				
									 	bodyn = { 
                                                	         		 'keywords' => kw_update_request
                                                      				}
                                                     		 	@result = threesixty_api( @apitoken.to_s, @access_token, "keyword", "update", bodyn)
									@update_response = @result["keyword_update_response"]["affectedRecords"]
									#logger.info @update_response
									delta = delta + 1
									totalchanges = totalchanges + 1
								end
								#rescue Exception
								#	 logger.info Exception
								#end 
								end 
						
						
						if delta > 0
						then 
							logger.info "||||| Total Three Sixty Adgroup Changes |||||"
							logger.info adgroupid 
							logger.info totalchanges
						end 
						end
			end 
		end

		logger.info "||||| Total Three Sixty Changes |||||"
		logger.info totalchanges
end 
end
