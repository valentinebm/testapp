class ChrisController < ApplicationController

  def test

    user_id = 13
    user = User.find(user_id)
    response = ''

    if user.channel = "360"

      login_info  = threesixty_login(user.username, user.password, user.apitoken, user.apisecret)
      access_token = login_info["account_clientLogin_response"]["accessToken"]

      if !access_token.nil?

        # result = threesixty_api( @apitoken.to_s, @access_token, "account", "getInfo")
        campaignIdList = threesixty_api(user.apitoken , access_token, "account", "getCampaignIdList")
        items = campaignIdList["account_getCampaignIdList_response"]["campaignIdList"]["item"]

        items.each do |campaignid|

          body = {}
          body[:campaignId] = campaignid
          response = threesixty_api(user.apitoken, access_token, "group", "getIdListByCampaignId", body)
          adeqoGroupIds = response["group_getIdListByCampaignId_response"]["groupIdList"]["item"]

          adeqoGroupIds.each do |adeqoGroupId|

            body = {}
            body[:groupId] = adeqoGroupId.to_i
            response = threesixty_api(user.apitoken, access_token, "keyword", "getIdListByGroupId", body)

            keyword_id_arr = []
            keyword_id_arr_str = ""

            keywordIdLists = response["keyword_getIdListByGroupId_response"]["keywordIdList"]["item"]

            if keywordIdLists.is_a?(Array)
              if keywordIdLists.count.to_i > 0
                keywordIdLists.each do |keyword_id_status_body_d|
                  keyword_id_arr << keyword_id_status_body_d.to_i
                end
              end
            else
              keyword_id_arr << keywordIdLists.to_i
            end

            if keyword_id_arr.count > 0
              keyword_id_arr_str = keyword_id_arr.join(",")

              body = {}
              body[:idList] = "["+keyword_id_arr_str+"]"

              response = threesixty_api(user.apitoken, access_token, "keyword", "getInfoByIdList", body)
              kwarr= response["keyword_getInfoByIdList_response"]["keywordList"]["item"]
            end

            break
          end

          break
        end

      end

    end

    data = {
      :message => "test by chris",
      :status => "true",
      :user => user,
      :login_info => login_info,
      # :campaignIdList => campaignIdList,
      # :items => items,
      :response => response
    }
    return render :json => data, :status => :ok

  end

  def threesixty_login(username, password,api_key, api_secret)
    cipher_aes = OpenSSL::Cipher::AES.new(128, :CBC)
    cipher_aes.encrypt
    cipher_aes.key = api_secret[0,16]
    cipher_aes.iv = api_secret[16,16]
    encrypted = (cipher_aes.update(Digest::MD5.hexdigest(password)) + cipher_aes.final).unpack('H*').join
    url = "https://api.e.360.cn/account/clientLogin"
    response = HTTParty.post(url,
      :timeout => 300,
      :body => {
        :username => username,
        :passwd => encrypted[0,64]
      },
      :headers => {'apiKey' => api_key }
    )
    return response.parsed_response
  end

  def threesixty_api( api_key, access_token, service, method, params = {})
    url = "https://api.e.360.cn/2.0/#{service}/#{method}"
    response = HTTParty.post(url,
    timeout: 300,
    body: params,
    headers: {
      'apiKey' => api_key,
      'accessToken' => access_token,
      'serveToken' => Time.now.to_i.to_s
    })

    # @response = response
    return response.parsed_response
  end
end
