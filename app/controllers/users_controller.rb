class UsersController < ApplicationController

require "threesixty.rb"
require "sogou.rb" # not work yet, to fix

def index
	@users = User.all
end

def new
	@user = User.new
end

def create
	@user = User.new(params.require(:comments).permit(:commenter, :body))
	if @user.save
		redirect_to @user, alert: "User created successfully."
	else
		redirect_to new_user_path, alert: "Error creating user."
	end
end

def show
	@users = User.find(params[:id])
end

def updateaccount
	@user =  User.find(params[:id])
end

def deleteaccount
	@user = User.find(params[:id])
	if @user.destroy
		redirect_to @user, alert: "User Deleted successfully."
	else
		redirect_to new_user_path, alert: "Error deleting user."
	end
end

def error
	@error = Error.new(params[:object])
end 

#function takes a USER ID and finds what channel and then calls the corresponding channel ID

def downloadkwdump
	@user = User.find(params[:id])
	if @user.channel == "Sogou"
		sogou_update_kw(@user.username,@user.password,@user.apitoken)
		return
	end

	if @user.channel == "360"
		threesixty_update_kw(@user.username,@user.password,@user.apitoken, @user.apisecret)
		return
	end
end

def updatead 
	@user = User.find(params[:id])
	if @user.channel == "Sogou"
		sogou_update_ad(@user.username,@user.password,@user.apitoken)
		return
	end

	if @user.channel == "360"
		threesixty_update_ad(@user.username,@user.password,@user.apitoken, @user.apisecret)
		return
	end
end
 
#function to take a set of username, password and apitoken and output a list of URLs

def sogou_update_ad(username, password, apitoken)

	@username = username
	@password = password
	@apitoken = apitoken
	@group_id_array = []
	@ad_arr = []
	groupbegin = 0 # set 0 to start from the beginning
	totalchanges = 0

        sogou_api(@username, @password, @apitoken, "CpcGrpService")
        @update_status = @sogou_api.call(:get_all_cpc_grp_id)
	#@header = @update_status.header.to_hash
	#@msg = @header[:res_header][:desc]
	#@remain_quote = @header[:res_header][:rquota]
	#logger.info "[Sogou][AD] Quota [" + @remain_quote.to_s + "]"
	@update_status_body = @update_status.body.to_hash
	@result_active_grp = @update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]
	@result_active_grp.each do |el|
		if el[:cpc_grp_ids].is_a?(Array)
			el[:cpc_grp_ids].each do |id| @group_id_array << id end
		else
			@group_id_array << el[:cpc_grp_ids]
		end
	end
	sogou_api(@username, @password, @apitoken, "CpcIdeaService")
	#logger.info @group_id_array.size #22144
	#@group_id_array = ["238876896", "238895734", "215628567"] #index 20106, 20107(nil), 20108
	@group_id_array[groupbegin..-1].each_with_index do |groupid, index|
		changes = 0
		@index = index + 1 + groupbegin
		@info = "[Sogou|AD|" + @username + "] [" + groupid.to_s + "][" + @index.to_s + "/" + @group_id_array.size.to_s + "] "
		logger.info @info + "Begin"
		@update_status = @sogou_api.call(:get_cpc_idea_by_cpc_grp_id, message: { cpcGrpIds: groupid, getTemp: 0 }) 
		@header = @update_status.header.to_hash
		#logger.info @msg = @header[:res_header][:desc]
		@remain_quote = @header[:res_header][:rquota]
		@update_status_body = @update_status.body.to_hash
		@result_active_cpc = @update_status_body[:get_cpc_idea_by_cpc_grp_id_response][:cpc_grp_ideas]
		#logger.info "[Sogou][AD] " + @result_active_cpc.to_s
		if @result_active_cpc[:cpc_idea_types].nil? then next end
		if !@result_active_cpc[:cpc_idea_types].nil?
	 		#logger.info "[Sogou] CPC Size [" + @result_active_cpc[:cpc_idea_types].size.to_s + "]"
			if @result_active_cpc[:cpc_idea_types].is_a?(Array)
				@ad_arr = @result_active_cpc[:cpc_idea_types]
			else
				@ad_arr << @result_active_cpc[:cpc_idea_types]
			end
			#logger.info @info + "All AD Size: [" + @ad_arr.size.to_s + "] All AD: " + @ad_arr.to_s
			request_arr = []
			@ad_arr.each do |ad|
				#logger.info @info + "AD: " + ad.to_s
				request = {}
				if (ad[:visit_url]) && (ad[:visit_url].include? "adeqo")
					@new_url= URI.unescape(ad[:visit_url].scan(/=(http[^>]*)/).last.first)
					request[:cpcIdeaId] = ad[:cpc_idea_id]
					request[:visitUrl] = @new_url
				end
				if (ad[:mobile_visit_url]) && (ad[:mobile_visit_url].include? "adeqo")
					@new_url= URI.unescape(ad[:mobile_visit_url].scan(/=(http[^>]*)/).last.first)
					request[:cpcIdeaId] = ad[:cpc_idea_id]
					request[:mobileVisitUrl] = @new_url
				end
				if request.size.to_i == 0 then next end
				request_arr << request
				changes = changes + 1
				rescue Timeout::Error
					logger.info "Timeout... waiting 30s then trying again"
					sleep(30)
				redo
			end 
		end
		#logger.info @info + "Request Size: [" + request_arr.size.to_s + "] Request: " + request_arr.to_s
		if request_arr.size.to_i == 0 then next end
		request_arr.each_slice(500) do |sub_arr| # set a number less than max number of API
			@result = @sogou_api.call(:update_cpc_idea, message: {cpcIdeaTypes: sub_arr})
			@header = @result.header.to_hash
			@update_response = @header[:res_header][:desc]
			if @update_response.to_s.downcase == "success"
				changes = changes + sub_arr.size
			end
		end
		if changes > 0
			totalchanges = totalchanges + changes
			logger.info @info + "Changes [" + changes.to_s + "] End"
		end
		#return
	end
	logger.info @info + "Total Changes [" + totalchanges.to_s + "] End"
	#return

end

#function to take a set of username, password and apitoken and output a list of URLs

def sogou_update_kw(username, password, apitoken)

	@username = username
	@password = password
	@apitoken = apitoken
	@group_id_array = []
	@kw_arr = []
	groupbegin = 0 # set 0 to start from the beginning
	totalchanges = 0

	#@group_id_array = sogou_groupid(@username, @password, @apitoken) #not work yet, to fix
	#logger.info @group_id_array.size
	#return

        sogou_api(@username, @password, @apitoken, "CpcGrpService")
        @update_status = @sogou_api.call(:get_all_cpc_grp_id)
	#@header = @update_status.header.to_hash
	#@msg = @header[:res_header][:desc]
	#@remain_quote = @header[:res_header][:rquota]
	#logger.info "[Sogou][KW] Quota [" + @remain_quote.to_s + "]"
	@update_status_body = @update_status.body.to_hash
	@result_active_grp = @update_status_body[:get_all_cpc_grp_id_response][:cpc_plan_grp_ids]
	@result_active_grp.each do |el|
		if el[:cpc_grp_ids].is_a?(Array)
			el[:cpc_grp_ids].each do |id| @group_id_array << id end
		else
			@group_id_array << el[:cpc_grp_ids]
		end
	end
	sogou_api(@username, @password, @apitoken, "CpcService")
	#logger.info @group_id_array.size #22144
	#@group_id_array = ["238876896", "238895734", "215628567"] #index 20106, 20107(nil), 20108
	@group_id_array[groupbegin..-1].each_with_index do |groupid, index|
		changes = 0
		@index = index + 1 + groupbegin
		@info = "[Sogou|KW|" + @username + "] [" + groupid.to_s + "][" + @index.to_s + "/" + @group_id_array.size.to_s + "] "
		logger.info @info + "Begin"
		@update_status = @sogou_api.call(:get_cpc_by_cpc_grp_id, message: { cpcGrpIds: groupid, getTemp: 0 }) 
		@header = @update_status.header.to_hash
		#logger.info @msg = @header[:res_header][:desc]
		@remain_quote = @header[:res_header][:rquota]
		@update_status_body = @update_status.body.to_hash
		@result_active_cpc = @update_status_body[:get_cpc_by_cpc_grp_id_response][:cpc_grp_cpcs]
		#logger.info "[Sogou][KW] " + @result_active_cpc.to_s
		if @result_active_cpc[:cpc_types].nil? then next end
		if !@result_active_cpc[:cpc_types].nil?
	 		#logger.info "[Sogou] CPC Size [" + @result_active_cpc[:cpc_types].size.to_s + "]"
			if !@result_active_cpc[:cpc_types].is_a?(Array)
				@kw_arr << @result_active_cpc[:cpc_types]
			else
				@kw_arr = @result_active_cpc[:cpc_types]
			end
			#logger.info @info + "All KW Size: [" + @kw_arr.size.to_s + "] All KW: " + @kw_arr.to_s
			#return
			request_arr = []
			@kw_arr.each do |kw|
				#logger.info @info + "KW: " + kw.to_s
				request = {}
				if (kw[:visit_url]) && (kw[:visit_url].include? "adeqo")
					@new_url= URI.unescape(kw[:visit_url].scan(/=(http[^>]*)/).last.first)
					request[:cpcId] = kw[:cpc_id]
					request[:visitUrl] = @new_url
				end
				if (kw[:mobile_visit_url]) && (kw[:mobile_visit_url].include? "adeqo")
					@new_url= URI.unescape(kw[:mobile_visit_url].scan(/=(http[^>]*)/).last.first)
					request[:cpcId] = kw[:cpc_id]
					request[:mobileVisitUrl] = @new_url
				end
				if request.size.to_i == 0 then next end
				request_arr << request
				changes = changes + 1
				rescue Timeout::Error
					logger.info "Timeout... waiting 30s then trying again"
					sleep(30)
				redo
			end 
		end
		#logger.info @info + "Request Size: [" + request_arr.size.to_s + "] Request: " + request_arr.to_s
		if request_arr.size.to_i == 0 then next end
		request_arr.each_slice(500) do |sub_arr| # set a number less than max number of API
			@result = @sogou_api.call(:update_cpc, message: {cpcTypes: sub_arr})
			@header = @result.header.to_hash
			@update_response = @header[:res_header][:desc]
			if @update_response.to_s.downcase == "success"
				changes = changes + sub_arr.size
			end
		end
		if changes > 0
			totalchanges = totalchanges + changes
			logger.info @info + "Changes [" + changes.to_s + "] End"
		end
		#return
	end
	logger.info @info + "Total Changes [" + totalchanges.to_s + "] End"
	#return

=begin
	rescue
	logger.info "-----Error Class-----"
	logger.info $!.class
	logger.info $@.class
	logger.info "-----Error Message-----"
	logger.info $!
	logger.info $@[0]

	@error = []
	@error << $i.class << $i << $@.class << $@[0]
	render plain: @error
	#render plain: "hello"
	#render :json => @error
	#render :action => "error", :error => @error #not work yet
=end

end

#Function Takes a User Credential and then calls the ad and kw level. all functions in threesixty.rb Updates the KW URL

def threesixty_update_ad(username,password,apitoken,apisecret)

	campaigns_array = ['携程错拼_phrase']
	groups_array = ['booking_phrase','agoda_phrase']
	@username = username
	@password = password
	@apitoken = apitoken
	@apisecret = apisecret
	login_info = threesixty_login(@username,@password,@apitoken,@apisecret)
	@access_token = login_info["account_clientLogin_response"]["accessToken"]
	campaignbegin = 0 # set 0 to start from the beginning
	totalchanges = 0
	
	if !@access_token.nil?
		campaigndata = threesixty_campaignids(@apitoken.to_s,@access_token)
		#logger.info campaigndata
		if campaigndata.size.to_i == 0 then return end
		campaigndata[campaignbegin..-1].each_with_index do |campaignid, cindex|
			campaignname = ""	
			body = {} 	
			body[:idList] = "[" + campaignid.to_s + "]"
			@result = threesixty_api( @apitoken.to_s, @access_token, "campaign", "getInfoByIdList", body)
			if @result.nil? then next end
			@campaign_body= @result["campaign_getInfoByIdList_response"]["campaignList"]["item"]
			campaignname = @campaign_body["name"]
			#if campaigns_array.exclude?(campaignname) then next end # to run campaigns in campaigns_array only
			@group_id_arr = []
			@group_id_arr = threesixty_adgroupid(@apitoken.to_s,@access_token,campaignid)
			if @group_id_arr.nil? then next end 
			@group_id_arr.each_with_index do |group_id, gindex|
				changes = 0
				groupname = ""
				body = {}
				body[:idList] = "[" + group_id.to_s + "]"
				@result = threesixty_api( @apitoken.to_s, @access_token, "group", "getInfoByIdList", body)
				if @result.nil? then next end
				@group_body= @result["group_getInfoByIdList_response"]["groupList"]["item"]
				groupname = @group_body["name"]
				#if groups_array.exclude?(groupname) then next end # to run groups in groups_array only
				@cindex = cindex + 1 + campaignbegin
				@gindex = gindex + 1
				@cinfo = "[360|AD|" + @username + "] [" + campaignname + "][" + campaignid.to_s + "][" + @cindex.to_s + "/" + campaigndata.size.to_s + "] "
				@ginfo = "[" + groupname + "][" + group_id.to_s + "][" + @gindex.to_s + "/" + @group_id_arr.size.to_s + "] "
				@prefix = @cinfo + @ginfo
				logger.info @prefix + "Begin"
				#logger.info @prefix + body.to_s
				@result = threesixty_api( @apitoken.to_s, @access_token, "creative", "getIdListByGroupId", {:groupId => group_id})
				#logger.info @result
				if @result["creative_getIdListByGroupId_response"]["creativeIdList"].nil? then next end 

				#logger.info @result = threesixty_api( @apitoken.to_s, @access_token, "creative", "getInfoByIdList", body)

				@all_ad = []

				#logger.info "-----Creative ID-----" # more than 1 element
				@ad_creatives = @result["creative_getIdListByGroupId_response"]["creativeIdList"]["item"]
				#logger.info @ad_creatives
				#return

				body = {}
				if @ad_creatives.is_a?(Array)
					@ad_creatives.each do |id|
						body[:idList] = "[" + id.to_s + "]"
						@result = threesixty_api( @apitoken.to_s, @access_token, "creative", "getInfoByIdList", body)
						@creative_body = @result["creative_getInfoByIdList_response"]["creativeList"]["item"]
						#logger.info "-----Creative Info-----"
						#logger.info @result
						#logger.info "-----Creative Body-----"
						#logger.info @creative_body
						@all_ad << @creative_body
					end
				else
					body[:idList] = "[" + @ad_creatives.to_s + "]"
					@result = threesixty_api( @apitoken.to_s, @access_token, "creative", "getInfoByIdList", body)
					@creative_body = @result["creative_getInfoByIdList_response"]["creativeList"]["item"]
					#logger.info "-----Creative Info-----"
					#logger.info @result
					#logger.info "-----Creative Body-----"
					#logger.info @creative_body
					@all_ad << @creative_body
				end
				#logger.info @prefix + "All AD Size: [" + @all_ad.size.to_s + "] All AD: " + @all_ad.to_s
				#return
				request_arr = []
				bodyn = {}				
				if @all_ad.nil? then next end  					
				@all_ad.each do |ad|
					#logger.info @prefix + "AD: " + ad.to_s
					#return
					if (ad["destinationUrl"]) && (ad["destinationUrl"].include? "adeqo") && (ad["destinationUrl"].include? "ctrip")
						@new_url= URI.unescape(ad["destinationUrl"].scan(/=(http[^>]*)/).last.first)
						request_str_u = ',"destinationUrl":"' + @new_url + '"'
					end
					if (ad["mobileDestinationUrl"]) && (ad["mobileDestinationUrl"].include? "adeqo") && (ad["mobileDestinationUrl"].include? "ctrip")
						@new_url= URI.unescape(ad["mobileDestinationUrl"].scan(/=(http[^>]*)/).last.first)
						request_str_m = ',"mobileDestinationUrl":"' + @new_url + '"'
					end
					if request_str_u || request_str_m
						request_str = '{"id":' + ad["id"].to_s + request_str_u.to_s + request_str_m.to_s + '}'
						request_arr << request_str
					end
					rescue Timeout::Error
						logger.info "Timeout... waiting 30s then trying again"
						sleep(30)
					redo
				end
				#logger.info @prefix + "Request Size: [" + request_arr.size.to_s + "] Request: " + request_arr.to_s
				if request_arr.size.to_i == 0 then next end	
				request_arr.each_slice(500) do |sub_arr| # set a number less than max number of API
					#logger.info @prefix + "Sub Array Size: [ "+ sub_arr.size.to_s + " ] Sub Array: " + sub_arr.to_s
					ad_update_request = '['+request_arr.join(",")+']'
					#logger.info @prefix + "Update Request: " + ad_update_request.to_s
					bodyn = { 'creatives' => ad_update_request }
					@result = threesixty_api( @apitoken.to_s, @access_token, "creative", "update", bodyn)
					#logger.info @prefix + "Update Response: " + @result["creative_update_response"].to_s
					@update_response = @result["creative_update_response"]["affectedRecords"]
					changes = changes + @update_response.to_i
				end
				if changes > 0
					totalchanges = totalchanges + changes
					logger.info @prefix + "Changes [" + changes.to_s + "] End"
				end
				#return
			end
			logger.info @cinfo + "Total Changes [" + totalchanges.to_s + "] End"
			#return
		end
	end
end 


#Function Takes a User Credential and then calls the ad and kw level. all functions in threesixty.rb Updates the KW URL

def threesixty_update_kw(username,password,apitoken,apisecret)

	campaigns_array = ['N哪到哪_机票_exact']
	groups_array = ['国考酒店','国考酒店-p']
	@username = username
	@password = password
	@apitoken = apitoken
	@apisecret = apisecret
	login_info = threesixty_login(@username,@password,@apitoken,@apisecret)
	@access_token = login_info["account_clientLogin_response"]["accessToken"]
	campaignbegin = 0 # set 0 to start from the beginning
	totalchanges = 0
	
	if !@access_token.nil?
		campaigndata = threesixty_campaignids(@apitoken.to_s,@access_token)
		#logger.info campaigndata
		if campaigndata.size.to_i == 0 then return end
		campaigndata[campaignbegin..-1].each_with_index do |campaignid, cindex|
			campaignname = ""	
			body = {} 	
			body[:idList] = "[" + campaignid.to_s + "]"
			@result = threesixty_api( @apitoken.to_s, @access_token, "campaign", "getInfoByIdList", body)
			if @result.nil? then next end
			@campaign_body= @result["campaign_getInfoByIdList_response"]["campaignList"]["item"]
			campaignname = @campaign_body["name"]
			#if campaigns_array.exclude?(campaignname) then next end # to run campaigns in campaigns_array only
			@group_id_arr = []
			@group_id_arr = threesixty_adgroupid(@apitoken.to_s,@access_token,campaignid)
			if @group_id_arr.nil? then next end 
			@group_id_arr.each_with_index do |group_id, gindex|
				changes = 0
				groupname = ""
				body = {}
				body[:idList] = "[" + group_id.to_s + "]"
				@result = threesixty_api( @apitoken.to_s, @access_token, "group", "getInfoByIdList", body)
				if @result.nil? then next end
				@group_body= @result["group_getInfoByIdList_response"]["groupList"]["item"]
				groupname = @group_body["name"]
				#if groups_array.exclude?(groupname) then next end # to run groups in groups_array only
				@cindex = cindex + 1 + campaignbegin
				@gindex = gindex + 1
				@cinfo = "[360|KW|" + @username + "] [" + campaignname + "][" + campaignid.to_s + "][" + @cindex.to_s + "/" + campaigndata.size.to_s + "] "
				@ginfo = "[" + groupname + "][" + group_id.to_s + "][" + @gindex.to_s + "/" + @group_id_arr.size.to_s + "] "
				@prefix = @cinfo + @ginfo
				logger.info @prefix + "Begin"
				#logger.info @prefix + body.to_s
				@all_kw = []
				@all_kw = threesixty_keywordid(@apitoken.to_s,@access_token,group_id)  
				#logger.info @prefix + "All Keyword Size: [" + @all_kw.size.to_s + "] All Keyword: " + @all_kw.to_s # +
				#return
				request_arr = []
				bodyn = {}
				if @all_kw.nil? then next end  					
				@all_kw.each do |kw|
					#logger.info @prefix + "Keyword: " + kw.to_s
					#return
					if (kw["destinationUrl"]) && (kw["destinationUrl"].include? "adeqo")
						@new_url= URI.unescape(kw["destinationUrl"].scan(/=(http[^>]*)/).last.first)
						request_str_u = ',"url":"' + @new_url + '"'
					end
					if (kw["mobileDestinationUrl"]) && (kw["mobileDestinationUrl"].include? "adeqo")
						@new_url= URI.unescape(kw["mobileDestinationUrl"].scan(/=(http[^>]*)/).last.first)
						request_str_m = ',"mobileUrl":"' + @new_url + '"'
					end
					if request_str_u || request_str_m
						request_str = '{"id":' + kw["id"].to_s + request_str_u.to_s + request_str_m.to_s + '}'
						request_arr << request_str
					end
					rescue Timeout::Error
						logger.info "Timeout... waiting 30s then trying again"
						sleep(30)
					redo
				end
				#logger.info @prefix + "Request Size: [" + request_arr.size.to_s + "] Request: " + request_arr.to_s
				if request_arr.size.to_i == 0 then next end
				request_arr.each_slice(500) do |sub_arr| # set a number less than max number of API
					#logger.info @prefix + "Sub Array Size: [ "+ sub_arr.size.to_s + " ] Sub Array: " + sub_arr.to_s
					kw_update_request = '['+sub_arr.join(",")+']'
					#logger.info @prefix + "Update Request: " + kw_update_request.to_s
					bodyn = { 'keywords' => kw_update_request }
					@result = threesixty_api( @apitoken.to_s, @access_token, "keyword", "update", bodyn)
					#logger.info @prefix + "Update Response: " + @result["keyword_update_response"].to_s
					@update_response = @result["keyword_update_response"]["affectedRecords"]
					changes = changes + @update_response.to_i
				end
				if changes > 0
					totalchanges = totalchanges + changes
					logger.info @prefix + "Changes [" + changes.to_s + "] End"
				end 
				#return
			end
			logger.info @cinfo + "Total Changes [" + totalchanges.to_s + "] End"
			#return
		end 
	end
end 

end
