#threesixty api

def threesixty_api( api_key, access_token, service, method, params = {})
        url = "https://api.e.360.cn/2.0/#{service}/#{method}"
        response = HTTParty.post(url,
                                    timeout: 300,
                                    body: params,
                                headers: {
                                        'apiKey' => api_key,
                                         'accessToken' => access_token,
                                        'serveToken' => Time.now.to_i.to_s
                                })
        @response = response
        if @response.headers["quotaremain"].to_i < 500
                then logger.info "|||||Not Enough Quota||||"
                return nil
        else
        return response.parsed_response
        end
end

def threesixty_login(username,password,api_key,api_secret)
                cipher_aes = OpenSSL::Cipher::AES.new(128, :CBC)
                cipher_aes.encrypt
                cipher_aes.key = api_secret[0,16]
                cipher_aes.iv = api_secret[16,16]
                encrypted = (cipher_aes.update(Digest::MD5.hexdigest(password)) + cipher_aes.final).unpack('H*').join
                url = "https://api.e.360.cn/account/clientLogin"
                response = HTTParty.post(url,
                :timeout => 300,
                :body => {
                        :username => username,
                        :passwd => encrypted[0,64]
                },
                :headers => {'apiKey' => api_key }
                 )
                return response.parsed_response
                end

def threesixty_campaignname(apitoken, access_token, campaignid)

	campaign_name = ""
	body = {}
	body[:idList] = "["+campaignid.to_s+"]"
	result = threesixty_api( apitoken.to_s, access_token, "campaign", "getInfoByIdList", body)
	if result.nil? then return nil end
	campaign_body= result["campaign_getInfoByIdList_response"]["campaignList"]["item"]
	campaign_name = campaign_body["name"]
	return campaign_name
end

def threesixty_groupname(apitoken, access_token, groupid)

	group_name = ""
	body = {}
	body[:idList] = "[" + groupid.to_s + "]"
	result = threesixty_api( apitoken.to_s, access_token, "group", "getInfoByIdList", body)
	if result.nil? then return nil end
	group_body= result["group_getInfoByIdList_response"]["groupList"]["item"]
	group_name = group_body["name"]
	return group_name
end

#pass apitoken, accesstoken, and gets a data structure of all campaignsid in an array.
#returns an array of campaignids

def threesixty_campaignids(apitoken, access_token)
	all_campaigns = []
	campaign_array = []
	result = threesixty_api(apitoken.to_s,access_token,"account", "getCampaignIdList")
	if result.nil? then return nil end 
	campaign_array = result["account_getCampaignIdList_response"]["campaignIdList"]["item"]
	if campaign_array.is_a?(Hash)
		if !campaign_array.empty?
                     all_campaigns << campaign_array
                     end
                else
                    all_campaigns = all_campaigns + campaign_array
                end
	if all_campaigns.count.to_i > 0
		return all_campaigns
	else 
		return nil

	end
end

#pass a campaign ID, apitoken, access token, return an array of adgroups ID

def threesixty_adgroupid(apitoken, access_token,campaignid)
			        adgroup_id_arr = []
                                adgroup_id_arr_str = ""
                                body = {}
                                body[:campaignId] = campaignid
                                result = threesixty_api( apitoken.to_s, access_token, "group", "getIdListByCampaignId", body)
                                if result.nil? then return nil end
                                adgroups = result["group_getIdListByCampaignId_response"]["groupIdList"]["item"]

                                if adgroups.is_a?(Array)
                                        if adgroups.count.to_i > 0
                                                  adgroups.each do |adgroups_d|
                                                      adgroup_id_arr << adgroups_d.to_i
                                                  end
                                              end
                                          else
                                              adgroup_id_arr << adgroups.to_i
                                          end
                                if adgroup_id_arr.count.to_i > 0 
					return adgroup_id_arr
				else
					return nil
				end 
end

#pass a apitoken , access token and an adgroup id ; returns a hashtable of keywords

def threesixty_keywordid(apitoken, access_token,adgroupid)
					
  					body = {}
                                       	body[:groupId] = adgroupid.to_i
                                        @result = threesixty_api( apitoken.to_s, access_token, "keyword", "getIdListByGroupId", body)
                           		if @result.nil? then return nil end 
					all_keyword = []
                                        keyword_id_arr = []
                                        keyword_id_arr_str = ""
                                        if @result["keyword_getIdListByGroupId_response"]["failures"].nil?
                                                        if !@result["keyword_getIdListByGroupId_response"]["keywordIdList"].nil?
                                                                if !@result["keyword_getIdListByGroupId_response"]["keywordIdList"]["item"].nil?
                                                                        @keyword_id_status_body = @result["keyword_getIdListByGroupId_response"]["keywordIdList"]["item"]
                                                                        if @keyword_id_status_body.is_a?(Array)
                                                                                if @keyword_id_status_body.count.to_i > 0
                                                                                      @keyword_id_status_body.each do |keyword_id_status_body_d|
                                                                                          keyword_id_arr << keyword_id_status_body_d.to_i
                                                                                        end
                                                                                end
                                                                        else
                                                                         keyword_id_arr << @keyword_id_status_body.to_i
                                                                        end
                                                                end
                                                        end
                                                        end
                                                if keyword_id_arr.count.to_i > 0
                                                        keyword_id_arr_str = keyword_id_arr.join(",")
                                                        body = {}
                                                        body[:idList] = "["+keyword_id_arr_str.to_s+"]"
                                                        @result = threesixty_api( apitoken.to_s, access_token, "keyword", "getInfoByIdList", body)
                                                        @kwarr= @result["keyword_getInfoByIdList_response"]["keywordList"]["item"]
                                                if @kwarr.is_a?(Hash)
                                                        if !@kwarr.empty?
                                                                all_keyword << @kwarr
                                                                end
                                                        else
                                                                all_keyword = all_keyword + @kwarr
                                                	end
						end 
					if all_keyword.count.to_i > 0
                	                        return all_keyword
	                                else
        	                                return nil
                        	        end

end



