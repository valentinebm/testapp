class WelcomeController < ApplicationController
skip_before_action :authenticate_adeqo_user!, :only => [:homepage]
  def index
  end

  def homepage
    render :layout => false
  end
end
