class UsersController < ApplicationController
require "threesixty.rb"

def index
	@users = User.all
end

def new
	@user = User.new
end

def create
   	@user = User.new(params[:user].permit!)
   	if @user.save
        	redirect_to @user, alert: "User created successfully."
   	else
       		redirect_to new_user_path, alert: "Error creating user."
    	end
end

def show
	@users = User.find(params[:id])
end

def updateaccount
	@user =  User.find(params[:id])
end

def deleteaccount
	@user = User.find(params[:id])
	if @user.destroy
                        redirect_to @user, alert: "User Deleted successfully."
                else
                       redirect_to new_user_path, alert: "Error deleting user."
                end
end

def error
	@error = Error.new(params[:object])
	

end 

#begin API calls


def sogou_login(username,password,token,api_string)
		@sogou_api = Savon.client(
     	 	wsdl: "http://api.agent.sogou.com:80/sem/sms/v1/"+api_string+"?wsdl",
     	 	pretty_print_xml: true,
     		log: true,
    		env_namespace: :soap,
	      		namespaces: {"xmlns:common" => "http://api.sogou.com/sem/common/v1"},
			soap_header: {
        		"common:AuthHeader" => {
          		'common:token' => token,
          		'common:username' => username,
          		'common:password' => password
       		 	}
      		}
    	)
  	end
=begin
def threesixty_login(username,password,api_key,api_secret)
		cipher_aes = OpenSSL::Cipher::AES.new(128, :CBC)
   		cipher_aes.encrypt
   		cipher_aes.key = api_secret[0,16]
   		cipher_aes.iv = api_secret[16,16]
   		encrypted = (cipher_aes.update(Digest::MD5.hexdigest(password)) + cipher_aes.final).unpack('H*').join
   		url = "https://api.e.360.cn/account/clientLogin"
		response = HTTParty.post(url,
       		:timeout => 300,
       		:body => {
     		  	:username => username,
       			:passwd => encrypted[0,64]
       		},
       		:headers => {'apiKey' => api_key }
  		 )
   		return response.parsed_response
		end

#threexity api call using HTTParty

def threesixty_api( api_key, access_token, service, method, params = {})
    url = "https://api.e.360.cn/2.0/#{service}/#{method}"
      response = HTTParty.post(url,
            timeout: 300,
            body: params,
            headers: {
                        'apiKey' => api_key,
                        'accessToken' => access_token,
                        'serveToken' => Time.now.to_i.to_s
                      })
#
      @response = response
      return response.parsed_response
end
=end 
#sogou api call using Savon

def sogou_api(username,password,token,api_string)            
    @sogou_api = Savon.client(
      wsdl: "http://api.agent.sogou.com:80/sem/sms/v1/"+api_string+"?wsdl",
      pretty_print_xml: true,
      log: true,
      env_namespace: :soap,
      namespaces: {"xmlns:common" => "http://api.sogou.com/sem/common/v1"},
      soap_header: { 
        "common:AuthHeader" => {
          'common:token' => token,
          'common:username' => username,
          'common:password' => password
        }
      }
    )    
  end


#function takes a USER ID and finds what channel and then calls the corresponding channel ID

def downloadkwdump
                @user = User.find(params[:id])
                if @user.channel == "Sogou"
		  update_sogou(@user.username,@user.password,@user.apitoken)
                        return
                end

                if @user.channel == "360"
                        update_threesixty(@user.username,@user.password,@user.apitoken, @user.apisecret)

                        return
                end

end

#to be built. (working on it)
 
def update_sogou(username,password,api_token)

	@username = username
	@password = password
	@api_token = api_token
	@campaign_id = params[:id]

=begin
##tofix

	#logger.info @campaign_id
	if @campaign_id.nil?
	    @current_campaign = @db[:all_campaign].find({ "$and" => [{:api_update => 4}, {:network_type => 'sogou'}, {:api_worker => @port.to_i}] })
	    @db.close
        
	    if @current_campaign.count.to_i >= 1
	        @logger.info "working, no need update sogou api campaign"
	        return render :nothing => true
	    end
        
	    @campaign = @db[:all_campaign].find({ "$and" => [{:api_update => 3}, {:network_type => 'sogou'}, {:api_worker => @port.to_i}] }).sort({ last_update: -1 }).limit(1)
	    @db.close
        
	    if @campaign.count.to_i == 0
	        @logger.info "no need update sogou api campaign"
	        return render :nothing => true
	    end
        
	else
	    @campaign = @db[:all_campaign].find({ "$and" => [{:cpc_plan_id => @campaign_id.to_i}, {:network_type => 'sogou'}] })
	    @db.close
	end

=end

=begin
##done

	logger.info "-----login info below-----"
	login_info = sogou_login(@username, @password, @api_token, "AccountDownloadService") 
	logger.info login_info 
	logger.info "-----login info above-----"

=end

	logger.info "-----AccountService begin-----"

        @sogou_result = sogou_api(@username, @password, @api_token, "AccountService")
	#logger.info @sogou_result

	@acc_info = @sogou_result.call(:get_account_info)
	#logger.info @acc_info

=begin
##tofix

	if sogou_result.header[:res_header][:desc].to_s == "success" && sogou_result.header[:res_header][:rquota].to_i >= 500
                    
	    @remain_quote = sogou_result.header[:res_header][:rquota].to_i
                
	    db_name = "adgroup_sogou_"+@network_id.to_s
             
	    @adgroup = @sogou_db[db_name].find({ "$and" => [{:cpc_plan_id => @campaign_id.to_i}, {:api_update_ad => 1}, {:api_update_keyword => 1}, {:api_worker => @port.to_i}] })
	    @sogou_db.close()
                    
	    @adgroup_id_array = []

=end


	logger.info "-----CpcPlanService begin-----"
        @result = sogou_api(@username, @password, @api_token, "CpcPlanService")
	logger.info @result

	#requesttypearray = []
	#requesttype = {}
	#requesttypearray << @campaign_id.to_i
	#logger.info "-----campaign id is-----"
	#logger.info @campaign_id
	#logger.info "-----request type is-----"
	#logger.info requesttypearray
	##campaign id is not used yet so far
	#@plan_info = @result.call(:get_cpc_plan_by_cpc_plan_id, message: { cpcPlanIds: requesttypearray })

	@plan_id = @result.call(:get_all_cpc_plan_id)
	@header = @plan_id.header.to_hash
	logger.info "-----plan id header is-----"
	logger.info @header
	@msg = @header[:res_header][:desc]
	logger.info "-----plan id header desc is-----"
	logger.info @msg
	@remain_quote = @header[:res_header][:rquota]
	logger.info "-----plan id header rquota is-----"
	logger.info @remain_quote
	@plan_id = @plan_id.body.to_a[0][1].values[0]
	#logger.info @plan_id.class
	#logger.info "-----plan id array is-----"
	#logger.info @plan_id
	##plan id with index
	#@plan_id.each_with_index {|val, index| logger.info "#{val} => #{index}" }
	#logger.info "-----plan id is-----"
	#@plan_id.each do |id| logger.info id end

	@plan_info = @result.call(:get_all_cpc_plan)
	@header = @plan_info.header.to_hash
	logger.info "-----plan info header is-----"
	logger.info @header
	@msg = @header[:res_header][:desc]
	logger.info "-----plan info header desc is-----"
	logger.info @msg
	@remain_quote = @header[:res_header][:rquota]
	logger.info "-----plan info header rquota is-----"
	logger.info @remain_quote
	@plan_info = @plan_info.body.to_a[0][1].values[0]
	#logger.info @plan_info.class
	#logger.info "-----plan info array is-----"
	#logger.info @plan_info
	##plan id with name
	logger.info "-----plan id with name is-----"
	
	
	#@update_status_body = @plan_info[:get_cpc_plan_by_cpc_plan_id_response][:cpc_plan_types]



 @adgroup_id_array = []
	@plan_info.each do |el|
	    logger.info el[:cpc_plan_id] << " " << el[:cpc_plan_name]
			   	 @adgroup_id_array = []
	    ##tofix
	    ##logger.info el[:KW url]
	    ##try to render
	    ##render layout: false
	end
	logger.info "-----CpcPlanService end-----"

	#render :action => "error", :error => @plan_info
	#render :plain "hello"

	render :json => @update_status_body

=begin
done
	logger.info "-----CpcGrpService begin-----"
        @result = sogou_api(@username, @password, @api_token, "CpcGrpService")
	logger.info @result

        @grp_id = @sogou_api.call(:get_all_cpc_grp_id)
	@header = @grp_id.header.to_hash
	logger.info "-----group id header is-----"
	logger.info @header
	@msg = @header[:res_header][:desc]
	logger.info "-----group id header desc is-----"
	logger.info @msg
	@remain_quote = @header[:res_header][:rquota]
	logger.info "-----group id header rquota is-----"
	logger.info @remain_quote
	@grp_id = @grp_id.body.to_a[0][1].values[0]
	#logger.info @grp_id.class
	#logger.info "-----group id array is-----"
	#logger.info @grp_id
	##group id with index
	#@grp_id.each_with_index {|val, index| logger.info "#{val} => #{index}" }
	#logger.info "-----plan id with group id array is-----"
	@grp_id_arr = []
	@grp_id.each do |el|
	    #logger.info el[:cpc_plan_id]
	    #logger.info el[:cpc_grp_ids]
	    @grp_id_arr << el[:cpc_grp_ids]
	    #el[:cpc_grp_ids].each do |id|
	        #logger.info id
	    #end
	end
	logger.info "-----group id array in array is-----"
	logger.info @grp_id_arr
	logger.info "-----CpcGrpService end-----"

=end

=begin
##tofix

	logger.info "-----CpcService begin-----"
        @result = sogou_api(@username, @password, @api_token, "CpcService")
	logger.info @result

	#logger.info @grp_id_arr.class
        @cpc_info = @sogou_api.call(:get_cpc_by_cpc_grp_id, message: { cpcGrpIds: @grp_id_arr, getTemp: 0 })
	logger.info "-----cpc info below-----"
	logger.info @cpc_info
	logger.info "-----cpc info above-----"
	logger.info "-----CpcService end-----"

	logger.info "-----CpcIdeaService begin-----"
	@result = sogou_api(@username, @password, @api_token, "CpcIdeaService")
	requesttypearray = [] 
	requesttype = {}
	requesttype[:cpcIdeaId] = all_ad_d[:cpc_idea_id].to_i
	requesttype[:cpcGrpId] = 0
	requesttype[:visitUrl] = @final_url
	requesttype[:mobileVisitUrl] = @m_final_url
                                                
	requesttypearray << requesttype
	 @logger.info requesttypearray
	@update_status = @sogou_api.call(:update_cpc_idea, message: { cpcIdeaTypes: requesttypearray })
                                                
	# @logger.info @update_status
                                                                         
	@header = @update_status.header.to_hash
	@msg = @header[:res_header][:desc]
	@remain_quote = @header[:res_header][:rquota]
                                                
	if @msg.to_s.downcase != "success"
	    @final_url = all_ad_d[:visit_url].to_s
	    @m_final_url = all_ad_d[:mobile_visit_url].to_s
	end

=end

	logger.info "-----AccountService end-----"

end

#Function Takes a User Credential and then calls the ad and kw level. Updates the KW URL

def update_threesixty(username,password,apitoken,apisecret)
		@username = username
		@password = password
		@apitoken = apitoken
		@apisecret = apisecret
		login_info = threesixty_login(@username,@password,@apitoken,@apisecret)
		@access_token = login_info["account_clientLogin_response"]["accessToken"]
		if !@access_token.nil?
			@result = threesixty_api( @apitoken.to_s, @access_token, "account", "getInfo")
			@result = threesixty_api( @apitoken.to_s, @access_token, "account", "getCampaignIdList")
			if @result.nil? then return end 
			campaigndata = @result["account_getCampaignIdList_response"]["campaignIdList"]["item"]
			#campaigndata = campaign_ids(@apitoken,@access_token)
			#if campaigndata.nil? then return end 
			#c = 0
			campaigndata.each do |campaignid|
			#if c < 6  
			#next 
			#end
			#c += 1 
			@adgroup_id_arr = []
                        @adgroup_id_arr_str = ""

			body = {}
                        body[:campaignId] = campaignid
#			logger.info campaignid
				@result = threesixty_api( @apitoken.to_s, @access_token, "group", "getIdListByCampaignId", body)
				if @result.nil? then next end 
				@adgroups = @result["group_getIdListByCampaignId_response"]["groupIdList"]["item"]

				if @adgroups.is_a?(Array)
                                    	if @adgroups.count.to_i > 0
                                                  @adgroups.each do |adgroups_d|
                                                      @adgroup_id_arr << adgroups_d.to_i
                                                  end
                                              end
                                          else
                                              @adgroup_id_arr << @adgroups.to_i
                                          end
				if @adgroup_id_arr.count.to_i == 0 then next end 
				@adgroup_id_arr.each do |id|
						body = {}
                                      		body[:groupId] = id.to_i
                                      		@result = threesixty_api( @apitoken.to_s, @access_token, "keyword", "getIdListByGroupId", body)
 				  		@all_keyword = []
						@keyword_id_arr = []
                                        	@keyword_id_arr_str = ""
						if @result["keyword_getIdListByGroupId_response"]["failures"].nil?
							if !@result["keyword_getIdListByGroupId_response"]["keywordIdList"].nil?
								if !@result["keyword_getIdListByGroupId_response"]["keywordIdList"]["item"].nil?
									@keyword_id_status_body = @result["keyword_getIdListByGroupId_response"]["keywordIdList"]["item"]
									if @keyword_id_status_body.is_a?(Array)
                                                		   	 	if @keyword_id_status_body.count.to_i > 0
                                                        			      @keyword_id_status_body.each do |keyword_id_status_body_d|
                                                                			  @keyword_id_arr << keyword_id_status_body_d.to_i
                                                        				end
                                               					end
                                         				else
                                                	         	 @keyword_id_arr << @keyword_id_status_body.to_i
                                        				end
								end
							end
							end 
						if @keyword_id_arr.count.to_i > 0
                         	                    	 @keyword_id_arr_str = @keyword_id_arr.join(",")
                                              		body = {}
                                             		body[:idList] = "["+@keyword_id_arr_str.to_s+"]"
                                        		@result = threesixty_api( @apitoken.to_s, @access_token, "keyword", "getInfoByIdList", body)
							@kwarr= @result["keyword_getInfoByIdList_response"]["keywordList"]["item"]
						if @kwarr.is_a?(Hash) 
							if !@kwarr.empty?
                                                      		@all_keyword << @kwarr
                                          		        end
                                              		else
                                                  		@all_keyword = @all_keyword + @kwarr		
						end
						if @all_keyword.count.to_i > 0  					
								@all_keyword.each do |key|
						#		logger.info key
								if key["destinationUrl"].include? "adeqo"	
									@new_url= URI.unescape(key["destinationUrl"].scan(/=(http[^>]*)/).last.first)
									#kw update function
									requesttypearray = []
									request_str = '{"id":'+key["id"].to_s+',"url":"'+@new_url+'"}'
									requesttypearray << request_str
                                                      			kw_update_request = '['+requesttypearray.join(",")+']'
										bodyn = {}				
									 	bodyn = { 
                                                	         		 'keywords' => kw_update_request
                                                      				}
                                                     		 	@result = threesixty_api( @apitoken.to_s, @access_token, "keyword", "update", bodyn)
						#			logger.info @result 
									@update_response = @result["keyword_update_response"]["affectedRecords"]
								end
								rescue Exception
									 logger.info "Exception"
								end 
						end 
					end

					end
				end

			end

		end
		end




