class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :channel
      t.string :username
      t.string :email
      t.string :password
      t.string :apitoken
      t.string :apisecret

      t.timestamps
    end
  end
end
