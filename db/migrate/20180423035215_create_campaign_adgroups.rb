class CreateCampaignAdgroups < ActiveRecord::Migration[5.2]
  def change
    create_table :campaign_adgroups do |t|
      t.string :campaign
      t.string :adgroup
      t.string :AccountId

      t.timestamps
    end
  end
end
