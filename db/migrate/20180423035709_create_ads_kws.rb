class CreateAdsKws < ActiveRecord::Migration[5.2]
  def change
    create_table :ads_kws do |t|
      t.string :adgroup
      t.string :ads
      t.string :keyword
      t.string :keyword_URL

      t.timestamps
    end
  end
end
