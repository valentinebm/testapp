class CreateCampaignAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :campaign_accounts do |t|
      t.string :campaign
      t.string :adgroup

      t.timestamps
    end
  end
end
