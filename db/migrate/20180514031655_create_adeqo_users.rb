class CreateAdeqoUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :adeqo_users do |t|
      t.string :username
      t.string :encrypted_password 
      t.string :salt

      t.timestamps
    end
  end
end
