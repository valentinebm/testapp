class CreateAccCampaigns < ActiveRecord::Migration[5.2]
  def change
    create_table :acc_campaigns do |t|
      t.string :account_name
      t.string :campaign

      t.timestamps
    end
  end
end
